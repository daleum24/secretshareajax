class Friendship < ActiveRecord::Base
  attr_accessible :in_friend_id, :out_friend_id

  validates :in_friend, :out_friend, presence: true

  belongs_to(
    :in_friend,
    class_name: "User",
    foreign_key: :in_friend_id,
    primary_key: :id,
    inverse_of: :in_friendships
  )

  belongs_to(
    :out_friend,
    class_name: "User",
    foreign_key: :out_friend_id,
    primary_key: :id,
    inverse_of: :out_friendships
  )

  def self.can_friend?(out_friend_id, in_friend_id)
    return false if out_friend_id == in_friend_id
    !Friendship.exists?(out_friend_id: out_friend_id, in_friend_id: in_friend_id)
  end

  def self.can_unfriend?(out_friend_id, in_friend_id)
    Friendship.exists?(out_friend_id: out_friend_id, in_friend_id: in_friend_id)
  end
end
