class SecretTagging < ActiveRecord::Base
  attr_accessible :secret_id, :tag_id

  validates :secret, :tag, presence: true

  belongs_to :secret, inverse_of: :secret_taggings
  belongs_to :tag, inverse_of: :secret_taggings
end
