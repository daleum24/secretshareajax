class FriendshipsController < ApplicationController
  def create

    @friendship = Friendship.new( out_friend_id: current_user.id,
                                  in_friend_id: params[:user_id])
    @friendship.save!

    head :ok
  end

  def destroy
    @friendship = Friendship.where(out_friend_id: current_user.id,
                                   in_friend_id: params[:user_id])
    @friendship[0].destroy unless @friendship.nil?

    head :ok
  end
end
