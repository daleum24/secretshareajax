class SecretsController < ApplicationController
  def new
    @secret = Secret.new
    @recipient_id = params[:user_id]
  end

  def create
    @secret = Secret.new(params[:secret])
    @secret.author_id = current_user.id

    if @secret.save
      render :json => {secret: @secret, tags: @secret.tags }
      # redirect_to user_url(params[:secret][:recipient_id])
    else
      head :bad_request
      # render :json => @secret.errors.full_messages
    end

  end
end
